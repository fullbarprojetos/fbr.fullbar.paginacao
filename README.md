# Fullbar Notification

Demo: https://codesandbox.io/s/vuejs-tutorial-e-exemplo-2cp57?file=/src/components/Paginacao.vue:381-428

# Instalação

```bash
npm install --save fbr.fullbar.paginacao
```

# Configurando

No arquivo **main.js** Importe o Pagination do 'fbr.fullbar.paginacao' e registre globalmente no app Vue, **Vue.component('Pagination', Pagination);**

```javascript
import Vue from "vue";

import App from "./app/App";

import Pagination from "fbr.fullbar.paginacao";
Vue.component("Pagination", Pagination);

new Vue({
  render: (h) => h(App),
}).$mount("#app");
```

# Como Usar

## Existem 2 propriedades obrigatórias para usar o componente

- **items** - O array que vai ser usado na paginação
- **changePage** - Função que fica observando a mudança dos eventos

```vue
<pagination :items="meuArray" @changePage="observaMudanca" />
```

## Existem também as propriedades opcionais:

- **pageSize** - O número de itens em cada página (o padrão é 10)
- **maxPages** - O número máximo de páginas a serem exibidas (o padrão é 10)
- **initialPage** - Em qual página vai começar (o padrão é a página 1)

```vue
<pagination :items="meuArray" @changePage="observaMudanca" :pageSize="5" :maxPages="50" :initialPage="1"
/>
```

# Style

O componente vem com CSS padrão bem básicos. Para usar seus próprio CSS personalizado, desative o CSS padrão, com a propriedade **:disableDefaultStyles="true"** e em seguida, use o CSS abaixo para personalizar:

- **.pagination** - Container pai
- **.pagination li** - Todos os itens da lista
- **.pagination li a** - Todos os links de paginação
- **.pagination li.page-number** - Todos os números da paginação
- **.pagination li.first** - O 'primeiro' elemento da paginação
- **.pagination li.last** - O 'último' elemento de paginação
- **.pagination li.previous** - O elemento da paginação 'Anterior'
- **.pagination li.next** - O elemento da paginação 'Próximo'

```javascript
<template>
        <pagination :items="meuArray" @changePage="observaMudanca"  :styles="meuCSS" />
</template>

<script>

const meuCSS = {
    ul: {
       margin: 0
    },
    li: {
        display: 'inline-block',
        border: 'margin-bottom: 10px'
    },
    a: {
        color: '#000'
    }
};

export default {
    data() {
        return {
            meuCSS
        };
    },
};
</script>
```

# Custom labels: (Primeiro, Ultimo, Proximo & Anterior)

## Você pode usar suas próprias labels para os botões Primeiro, Último, Próximo e Anterior, passando um objeto como este (não aceita img):

```javascript
<template>
   <pagination :items="meuArray" @changePage="observaMudanca" :labels="minhasLabels" />
</template>

<script>

const minhasLabels = {
    first: 'primeiro',
    last: 'ultimo',
    previous: '<',
    next: '>'
};

export default {
    data() {
        return {
            minhasLabels
        };
    },
};
</script>
```
