var path = require("path");
const VueLoaderPlugin = require("vue-loader/lib/plugin");

module.exports = {
  mode: "production",
  entry: "./src/Pagination.vue",
  output: {
    path: path.resolve("dist"),
    filename: "fbr.fullbar.paginacao.js",
    libraryTarget: "commonjs2",
  },
  module: {
    rules: [
      {
        test: /\.vue?$/,
        loader: "vue-loader",
      },
      {
        test: /\.js?$/,
        loader: "babel-loader",
      },
    ],
  },
  plugins: [new VueLoaderPlugin()],
};
